create table person
(
	id serial not null,
	firstname varchar default 255,
	lastname varchar default 255,
	city varchar default 255,
	phone varchar default 255,
	telegram varchar default 255
);

create unique index person_id_uindex
	on person (id);

alter table person
	add constraint person_pk
		primary key (id);

insert into Person values ('1','Aslan','Omirzak','Petropavlovsk','+77779214739','aslanomirzak');
insert into Person values ('2','Alisher','Arman','Kyzyl-Orda','+777777777','alish')